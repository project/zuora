<?php

interface ZuoraObjectInterface {
  public function toArray();
}
