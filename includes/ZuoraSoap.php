<?php


class ZuoraSoap extends Zuora {

  /**
   *
   */
  const ZUORA_SOAP_PRODUCTION = 'https://www.zuora.com/apps/services/a/68.0';

  /**
   *
   */
  const ZUORA_SOAP_SANDBOX = 'https://apisandbox.zuora.com/apps/services/a/68.0';

  /**
   * Generic namespace.
   */
  const TYPE_NAMESPACE = 'http://api.zuora.com/';

  /**
   * Defines the Zuora Object namespace, which is required for certain nodes in
   * the request.
   */
  const TYPE_OBJECT = 'http://object.api.zuora.com/';

  protected $wsdl;
  protected $client;
  protected $headers = array();

  /**
   * Singleton class instance.
   *
   * @var ZuoraSoap
   */
  protected static $instance;

  /**
   * Singleton constructor for Zuora API.
   *
   * @return \ZuoraSoap
   */
  public static function instance() {
    if (self::$instance === NULL) {
      self::$instance = new self();
    }

    return self::$instance;
  }

  /**
   * Sets the API credential information.
   *
   * @throws ZuoraException
   *
   */
  public function __construct() {
    parent::__construct();

    $this->wsdl = drupal_get_path('module', 'zuora') . '/includes/zuora.68.0.wsdl';

    $this->client = new SoapClient($this->wsdl, array(
      'soap_version' => SOAP_1_1,
      'trace' => 1,
    ));

    $this->createSession();
  }

  protected function setLocation() {
    $location = ($this->is_sandbox) ? self::ZUORA_SOAP_SANDBOX : self::ZUORA_SOAP_PRODUCTION;
    $this->client->__setLocation($location);
  }

  protected function createSession() {
    $this->setLocation();
    try {
      $result = $this->client->login(array(
        'username' => $this->tenant_user_id,
        'password' => $this->tenant_password,
      ));
    }
    catch (SoapFault $e) {
      throw new ZuoraException('Error authenticating to remote service');
    }

    $this->addHeader(new SoapHeader(
      'http://api.zuora.com/',
      'SessionHeader',
      array(
        'session'=>$result->result->Session
      )
    ));
  }

  public function addHeader(SoapHeader $soap_hader) {
    $this->headers[] = $soap_hader;
  }

  public function query($query) {
    $api_query = array(
      'query' => array(
        'queryString' => $query,
      )
    );

    try {
      $result = $this->call('query', $api_query, null, $this->headers);
    }
    catch (SoapFault $e) {
      throw new ZuoraException('Error executing remote API call: ' . $e->getMessage());
    }

    return $result;
  }

  /**
   * @param $function_name
   * @param array $arguments
   * @param array|NULL $options
   * @param null $input_headers
   * @param array|NULL $output_headers
   * @return mixed
   * @throws \SoapFault
   */
  public function call($function_name, array $arguments, array $options = null, $input_headers = null, array &$output_headers = null) {
    return $this->client->__soapCall($function_name, $arguments, $options, $input_headers, $output_headers);
  }

  /**
   * Process a subscribe call with the Soap client.
   *
   * @param $arguments array
   *   An array of an array of SoapVars of type 'subscribes'. It's like this
   *   because you pass in an array (which has all of the arguments to the call
   *   in it) and then another array inside of that which stores the subscribes.
   * @return object
   * @throws \SoapFault
   */
  public function subscribe($arguments) {
    return $this->call('subscribe', $arguments, null, $this->headers);
  }

  /**
   * Process a amend call with the Soap client.
   *
   * @param $arguments
   * @return mixed
   */
  public function amend($arguments){
    return $this->call('amend', $arguments, null, $this->headers);
  }

  /**
   * Process a create call with the Soap client.
   *
   * @param $arguments array
   *   An array of zObjects
   * @return object
   * @throws \SoapFault
   */
  public function create($arguments) {
    return $this->call('create', $arguments, null, $this->headers);
  }

  /**
   * Process a delete call with the Soap client.
   *
   * @param $arguments array
   *   An array of arguments.
   * @return object
   * @throws \SoapFault
   */
  public function delete($arguments) {
    return $this->call('delete', $arguments, null, $this->headers);
  }

  /**
   * Process an update call with the Soap client.
   *
   * @param $arguments array
   *   An array of zObjects
   * @return object
   * @throws \SoapFault
   */
  public function update($arguments) {
    return $this->call('update', $arguments, null, $this->headers);
  }

  /**
   * Returns the body of the last request this class sent.
   * @return string
   */
  public function lastRequest() {
    return $this->client->__getLastRequest();
  }

  /**
   * Returns the last response headers this class sent.
   * @return string
   */
  public function lastResponseHeaders(){
    return $this->client->__getLastResponseHeaders();
  }

  /**
   * Returns the last request headers this class sent.
   * @return string
   */
  public function lastRequestHeaders(){
    return $this->client->__getLastRequestHeaders();
  }

  /**
   *Returns the body of the last response this class sent.
   * @return string
   */
  public function lastResponse() {
    return $this->client->__getLastResponse();
  }
}
